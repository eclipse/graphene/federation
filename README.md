# Graphene Federation (archived)

This project is no longer active and has been superseeded by Federation4 

This repository supports Federation between Graphene system instances to synchronize catalog entries.

Please see the documentation in the "docs" folder.

